"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const mongo_1 = __importDefault(require("./config/mongo"));
require("dotenv/config");
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const compression_1 = __importDefault(require("compression"));
const mongoose_1 = __importDefault(require("mongoose"));
const morgan_1 = __importDefault(require("morgan"));
const swagger_ui_express_1 = __importDefault(require("swagger-ui-express"));
const swaggerDocument = __importStar(require("./config/swagger/swagger.json"));
const multer = require("multer");
const passport_1 = __importDefault(require("passport"));
const UserRoutes_1 = require("./routes/UserRoutes");
const AuthRoutes_1 = require("./routes/AuthRoutes");
const CampaignRoutes_1 = require("./routes/CampaignRoutes");
const CharacterRoutes_1 = require("./routes/CharacterRoutes");
var AWS = require("aws-sdk");
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
        this.mongo();
    }
    config() {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(morgan_1.default(":remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms"));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(compression_1.default());
        this.app.use(cors_1.default());
        this.app.use(passport_1.default.initialize());
        this.app.use(passport_1.default.session());
        require("./auth/passportHandler");
        this.app.use("/swagger", swagger_ui_express_1.default.serve, swagger_ui_express_1.default.setup(swaggerDocument));
    }
    routes() {
        this.app.use("/api/v1/auth", new AuthRoutes_1.AuthRoutes().router);
        this.app.use("/api/v1/users", new UserRoutes_1.UserRoutes().router);
        this.app.use("/api/v1/campaign", new CampaignRoutes_1.CampaignRoutes().router);
        this.app.use("/api/v1/character", new CharacterRoutes_1.CharacterRoutes().router);
    }
    mongo() {
        const connection = mongoose_1.default.connection;
        mongoose_1.default.set("useFindAndModify", false);
        connection.on("connected", () => {
            console.log("Mongo Connection Established");
        });
        connection.on("reconnected", () => {
            console.log("Mongo Connection Reestablished");
        });
        connection.on("disconnected", () => {
            console.log("Mongo Connection Disconnected");
        });
        connection.on("error", (error) => {
            console.log("Mongo Connection ERROR:" + error);
        });
        const run = () => __awaiter(this, void 0, void 0, function* () {
            yield mongoose_1.default.connect(mongo_1.default.connection, {
                autoReconnect: true, keepAlive: true, connectTimeoutMS: 10000, maxPoolSize: 10, autoIndex: true
            });
        });
        run().catch(error => console.log(error));
    }
    start() {
        this.app.listen(this.app.get("port"), () => {
            return console.log("\x1b[33m%s\x1b[0m", "Server :: Running @ 'http://localhost:'" + this.app.get("port"));
        });
    }
}
exports.Server = Server;
const server = new Server();
server.start();
//# sourceMappingURL=server.js.map