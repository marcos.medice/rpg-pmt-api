"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Character = exports.characterSchema = void 0;
const mongoose_1 = require("mongoose");
exports.characterSchema = new mongoose_1.Schema({
    userId: { type: mongoose_1.Schema.Types.ObjectId, required: true, ref: "User" },
    campaignId: { type: mongoose_1.Schema.Types.ObjectId, required: true, ref: "Campaign" },
    name: String,
    uuid: String,
    picture: {
        data: Buffer,
        contentType: String
    },
    race: String,
    class: String,
    level: {
        type: Number,
        default: 1
    },
    stats: {
        type: Object,
        default: {
            HP: undefined,
            strenght: undefined,
            constitution: undefined,
            dexterity: undefined,
            intelligence: undefined,
            wisdom: undefined,
            charisma: undefined
        }
    },
    equipment: {
        type: Object,
        default: {
            head: undefined,
            neck: undefined,
            body: undefined,
            leftHand: {
                weapon: undefined,
                ring: undefined
            },
            rightHand: {
                weapon: undefined,
                ring: undefined
            },
            pants: undefined,
            legs: undefined,
            feet: undefined,
            extra: [],
            pets: []
        },
    },
    inventory: [],
    creationDate: {
        type: Date,
        default: new Date()
    },
    updateDate: {
        type: Date,
        default: undefined
    },
    status: String
});
let options = {};
options.virtual = true;
exports.characterSchema.set("toJSON", options);
exports.characterSchema.pre("save", function save(next) {
    const character = this;
    next();
});
exports.Character = mongoose_1.model("Character", exports.characterSchema, "character");
//# sourceMappingURL=character.js.map