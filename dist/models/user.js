"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.userSchema = void 0;
const mongoose_1 = require("mongoose");
const bcrypt_1 = __importDefault(require("bcrypt"));
exports.userSchema = new mongoose_1.Schema({
    name: String,
    username: String,
    email: String,
    document: String,
    cellPhone: String,
    password: String,
    acceptUseTerms: Boolean,
    status: Boolean,
    activationToken: String,
    profilePicture: {
        data: Buffer,
        contentType: String
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    activationDate: {
        type: Date,
        default: undefined
    },
    updateDate: {
        type: Date,
        default: undefined
    }
});
let options;
options = {};
options.virtuals = true;
options.transform = function (doc, ret, options) {
    if (options.hide) {
        options.hide.split("password").forEach(function (prop) {
            delete ret[prop];
        });
    }
    return ret;
};
exports.userSchema.set("toJSON", options);
exports.userSchema.pre("save", function save(next) {
    const user = this;
    user.email = user.email.toLowerCase();
    next();
});
exports.userSchema.methods.comparePassword = function (candidatePassword, callback) {
    bcrypt_1.default.compare(candidatePassword, this.password, (err, isMatch) => {
        callback(err, isMatch);
    });
};
exports.User = mongoose_1.model("User", exports.userSchema, "users");
//# sourceMappingURL=user.js.map