"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserChangePassword = exports.userChangePasswordSchema = void 0;
const mongoose_1 = require("mongoose");
exports.userChangePasswordSchema = new mongoose_1.Schema({
    userId: String,
    token: String,
    status: Boolean,
    expireDate: {
        type: Date,
        default: undefined
    },
    creationDate: {
        type: Date,
        default: Date.now
    },
    updateDate: {
        type: Date,
        default: undefined
    }
});
let options;
options = {};
options.virtuals = true;
exports.userChangePasswordSchema.set("toJSON", options);
exports.userChangePasswordSchema.virtual("users", {
    ref: "Users",
    localField: "userId",
    foreignField: "_id",
    justOne: true
});
exports.UserChangePassword = mongoose_1.model("UserChangePassword", exports.userChangePasswordSchema, "userChangePasswordRequests");
//# sourceMappingURL=userChangePassword.js.map