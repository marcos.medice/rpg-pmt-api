"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.APIErrorResponse = void 0;
class APIErrorResponse {
    constructor() {
        this.errors = new Array();
        this.messages = new Array();
    }
}
exports.APIErrorResponse = APIErrorResponse;
//# sourceMappingURL=APIErrorResponse.js.map