"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Campaign = exports.campaignSchema = void 0;
const mongoose_1 = require("mongoose");
const bcrypt_1 = __importDefault(require("bcrypt"));
exports.campaignSchema = new mongoose_1.Schema({
    master: {
        type: mongoose_1.Schema.Types.ObjectId, required: false, ref: "User"
    },
    name: String,
    uuid: String,
    isPrivate: Boolean,
    password: String,
    playersIds: [{
            type: mongoose_1.Schema.Types.ObjectId, required: false, ref: "User"
        }],
    charactersIds: [{
            type: mongoose_1.Schema.Types.ObjectId, required: false, ref: "User"
        }],
    worldLevel: {
        type: Number,
        default: 1
    },
    status: {
        type: Boolean,
        default: false
    },
    creationDate: {
        type: Date,
        default: new Date()
    },
    updateDate: {
        type: Date,
        default: undefined
    }
});
let options = {};
options.virtuals = true;
options.transform = function (doc, ret, options) {
    if (options.hide) {
        options.hide.split("password").forEach(function (prop) {
            delete ret[prop];
        });
    }
    return ret;
};
exports.campaignSchema.set("toJSON", options);
exports.campaignSchema.pre("save", function save(next) {
    const campaign = this;
    next();
});
exports.campaignSchema.methods.comparePassword = function (candidatePassword, callback) {
    bcrypt_1.default.compare(candidatePassword, this.password, (err, isMatch) => {
        callback(err, isMatch);
    });
};
exports.Campaign = mongoose_1.model("Campaign", exports.campaignSchema, "campaigns");
//# sourceMappingURL=campaign.js.map