"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CampaignController = void 0;
const campaign_1 = require("../models/campaign");
const api_util_1 = require("../utils/api-util");
const messages_1 = require("../utils/messages");
const APIErrorResponse_1 = require("../models/utils/APIErrorResponse");
const uuid_1 = require("uuid");
class CampaignController {
    save(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                const campaign = new campaign_1.Campaign();
                campaign.master = req.body.id;
                campaign.name = req.body.name;
                campaign.uuid = uuid_1.v4();
                campaign.isPrivate = req.body.isPrivate;
                if (req.body.isPrivate) {
                    if (!req.body.password) {
                        res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_PASSWORD_NEEDED });
                        return;
                    }
                    campaign.password = req.body.password;
                }
                campaign.playersIds.push(req.body.id);
                campaign_1.Campaign.create(campaign);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.INTERNAL_SERVER_ERROR).json(responseError);
            }
        });
    }
}
exports.CampaignController = CampaignController;
//# sourceMappingURL=CampaignController.js.map