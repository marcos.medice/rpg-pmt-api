"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CharacterController = void 0;
const api_util_1 = require("../utils/api-util");
const messages_1 = require("../utils/messages");
const APIErrorResponse_1 = require("../models/utils/APIErrorResponse");
const character_1 = require("../models/character");
const campaign_1 = require("../models/campaign");
const user_1 = require("../models/user");
const uuid_1 = require("uuid");
const fs = __importStar(require("fs"));
class CharacterController {
    saveCharacter(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                if (!(yield user_1.User.findById(req.body.userId))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                if (!(yield campaign_1.Campaign.findById(req.body.campaignId))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_CAMPAIGN_NOT_FOUND });
                    return;
                }
                const character = new character_1.Character();
                character.userId = req.body.userId;
                character.campaignId = req.body.campaignId;
                character.name = req.body.name;
                character.uuid = uuid_1.v4();
                character.race = req.body.race;
                character.class = req.body.class;
                character_1.Character.create(character);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(error);
                res.status(api_util_1.APIUtil.INTERNAL_SERVER_ERROR).json({ status: "error", message: messages_1.Messages.GENERIC_ERROR_500_MESSAGE });
            }
        });
    }
    updateCharacterPicture(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                const character = yield character_1.Character.findOne({ _id: req.body.id });
                if (!character) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_CHARACTER_NOT_FOUND });
                    return;
                }
                character.picture.data = fs.readFileSync(req.file.path);
                character.picture.contentType = req.file.mimetype;
                character.updateDate = new Date();
                character.save();
                fs.unlinkSync(req.file.path);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
}
exports.CharacterController = CharacterController;
//# sourceMappingURL=CharacterController.js.map