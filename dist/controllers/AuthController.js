"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const api_util_1 = require("../utils/api-util");
require("../auth/passportHandler");
const passport_1 = __importDefault(require("passport"));
class AuthController {
    authenticateJWT(req, res, next) {
        passport_1.default.authenticate("jwt", function (err, user, info) {
            if (err) {
                return res.status(api_util_1.APIUtil.UNAUTHORIZED).json();
            }
            if (!user) {
                return res.status(api_util_1.APIUtil.UNAUTHORIZED).json();
            }
            else {
                req.user = user;
                return next();
            }
        })(req, res, next);
    }
    authorizeJWT(req, res, next) {
        passport_1.default.authenticate("jwt", (err, user, jwtToken) => {
            if (err) {
                return res.status(api_util_1.APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized" });
            }
            if (!user) {
                return res.status(api_util_1.APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized" });
            }
            else {
                const scope = req.baseUrl.split("/").slice(-1)[0];
                const authScope = jwtToken.scope;
                if (authScope && authScope.indexof(scope) > -1) {
                    return next();
                }
                else {
                    return res.status(api_util_1.APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized" });
                }
            }
        })(req, res, next);
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=AuthController.js.map