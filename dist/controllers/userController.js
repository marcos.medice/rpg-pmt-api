"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const user_1 = require("../models/user");
const userChangePassword_1 = require("../models/userChangePassword");
const bcrypt_1 = __importStar(require("bcrypt"));
const APIErrorResponse_1 = require("../models/utils/APIErrorResponse");
const api_util_1 = require("../utils/api-util");
const validations_1 = require("../utils/validations");
const messages_1 = require("../utils/messages");
const uuid_1 = require("uuid");
const passport_1 = __importDefault(require("passport"));
const jwt = __importStar(require("jsonwebtoken"));
const fs = __importStar(require("fs"));
const mailService_1 = __importDefault(require("../service/mailService"));
const multer = require("multer");
const secretConfig = require("../config/secret");
class UserController {
    saveUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            const validations = new validations_1.Validations();
            try {
                const user = new user_1.User();
                if (!(yield validations.validateEmail(req.body.email))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_EMAIL });
                    return;
                }
                if (!(yield validations.validateDocument(req.body.document))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_DOCUMENT });
                    return;
                }
                if (!(yield validations.validateStrongPassword(req.body.password))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_PASSWORD });
                    return;
                }
                if (!req.body.acceptUseTerms) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_TERMS_NOT_ACCEPTED });
                    return;
                }
                const query = { $or: [
                        { document: req.body.document },
                        { email: req.body.email },
                        { username: req.body.username },
                        { cellPhone: req.body.cellPhone }
                    ] };
                const isRegisterValid = yield user_1.User.find(query);
                if (isRegisterValid && isRegisterValid.length > 0) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_ALREADY_REGISTERED });
                    return;
                }
                user.name = req.body.name;
                user.username = req.body.username;
                user.email = req.body.email;
                user.document = req.body.document;
                user.cellPhone = req.body.cellPhone;
                user.password = bcrypt_1.default.hashSync(req.body.password, bcrypt_1.default.genSaltSync(10));
                user.acceptUseTerms = true;
                user.status = false;
                user.activationToken = uuid_1.v4();
                user_1.User.create(user);
                const mailService = new mailService_1.default();
                const activationUrl = "http://localhost:PORT/users/activate/";
                let htmlEmail = `${messages_1.Messages.MESSAGE_WELCOME} ${messages_1.Messages.MESSAGE_TO_ACTIVATE_YOUR_REGISTER_CLICK_HERE} ${activationUrl}${user.activationToken}`;
                yield mailService.run(req.body.email, messages_1.Messages.MESSAGE_WELCOME, htmlEmail);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.INTERNAL_SERVER_ERROR).json(responseError);
            }
        });
    }
    activateUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                const user = yield user_1.User.findOne({ activationToken: req.query.activationToken.toString(), status: false });
                if (!user) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                user.status = true;
                user.activationDate = new Date();
                user.activationToken = undefined;
                user.save();
                const mailService = new mailService_1.default();
                const platformUrl = "http://localhost:PORT/login";
                let htmlEmail = `${messages_1.Messages.MESSAGE_ACCOUNT_ACTIVATED_TEXT} ${platformUrl}`;
                yield mailService.run(user.email.toString(), messages_1.Messages.MESSAGE_ACCOUNT_ACTIVATED_SUBJECT, htmlEmail);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
    updateProfilePicture(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                const user = yield user_1.User.findOne({ _id: req.body.id, status: true });
                if (!user) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                user.profilePicture.data = fs.readFileSync(req.file.path);
                user.profilePicture.contentType = req.file.mimetype;
                user.updateDate = new Date();
                user.save();
                fs.unlinkSync(req.file.path);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
    changePasswordRequest(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            try {
                const user = yield user_1.User.findOne({ email: req.body.email, status: true });
                if (!user) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                const userChangePassword = new userChangePassword_1.UserChangePassword();
                userChangePassword.userId = user._id;
                userChangePassword.token = uuid_1.v4();
                userChangePassword.status = true;
                // userChangePassword.expireDate =
                userChangePassword_1.UserChangePassword.create(userChangePassword);
                const mailService = new mailService_1.default();
                const changePasswordUrl = "http://localhost:PORT/users/";
                let htmlEmail = `${messages_1.Messages.MESSAGE_TO_CHANGE_PASSWORD_CLICK_HERE} ${changePasswordUrl}${userChangePassword.token}/change-password`;
                yield mailService.run(req.body.email, messages_1.Messages.MESSAGE_CHANGE_PASSWORD_REQUEST, htmlEmail);
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
    updatePasswordFromRequest(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            const validations = new validations_1.Validations();
            try {
                if (!(yield validations.validateStrongPassword(req.body.password))) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_PASSWORD });
                    return;
                }
                const userChangePassword = yield userChangePassword_1.UserChangePassword
                    .findOne({ token: req.params.token, status: true });
                if (!userChangePassword) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error",
                        message: messages_1.Messages.MESSAGE_USER_CHANGE_PASSWORD_REQUEST_NOT_FOUND });
                    return;
                }
                const user = yield user_1.User.findOne({ _id: userChangePassword.userId, status: true });
                if (!user) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                userChangePassword.status = false;
                userChangePassword.expireDate = undefined;
                userChangePassword.updateDate = new Date();
                userChangePassword.save();
                user.password = bcrypt_1.default.hashSync(req.body.password, bcrypt_1.default.genSaltSync(10));
                user.updateDate = new Date();
                user.save();
                res.status(api_util_1.APIUtil.OK).json("ok");
                const mailService = new mailService_1.default();
                let htmlEmail = `${messages_1.Messages.MESSAGE_PASSWORD_CHANGED_TEXT} ${req.body.password}`;
                yield mailService.run(user.email.toString(), messages_1.Messages.MESSAGE_PASSWORD_CHANGED_SUBJECT, htmlEmail);
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
    updateUserProfile(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const responseError = new APIErrorResponse_1.APIErrorResponse();
            const validations = new validations_1.Validations();
            try {
                const user = yield user_1.User.findOne({ _id: req.body.id, status: true });
                if (!user) {
                    res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_USER_NOT_FOUND });
                    return;
                }
                if (req.body.password) {
                    if (!(yield validations.validateStrongPassword(req.body.password))) {
                        res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_PASSWORD });
                        return;
                    }
                    else {
                        user.password = bcrypt_1.default.hashSync(req.body.password, bcrypt_1.genSaltSync(10));
                    }
                }
                if (req.body.email) {
                    if (!(yield validations.validateEmail(req.body.email))) {
                        res.status(api_util_1.APIUtil.BAD_REQUEST).json({ status: "error", message: messages_1.Messages.MESSAGE_INVALID_EMAIL });
                        return;
                    }
                    else {
                        user.email = req.body.email;
                    }
                }
                if (req.body.username) {
                    user.username = req.body.username;
                }
                if (req.body.cellPhone) {
                    user.cellPhone = req.body.cellPhone;
                }
                // if (req.file) {
                //   user.profilePicture.data = fs.readFileSync(req.file.path);
                //   user.profilePicture.contentType = req.file.mimetype;
                //   fs.unlinkSync(req.file.path);
                // }
                user.updateDate = new Date();
                user.save();
                res.status(api_util_1.APIUtil.OK).json("ok");
            }
            catch (error) {
                responseError.errors.push(messages_1.Messages.GENERIC_ERROR_500_MESSAGE);
                res.status(api_util_1.APIUtil.BAD_REQUEST).json(error);
            }
        });
    }
    authenticateUser(req, res, next) {
        passport_1.default.authenticate("local", (err, user, info) => {
            if (err)
                return next(err);
            if (!user) {
                const responseError = new APIErrorResponse_1.APIErrorResponse();
                responseError.error = messages_1.Messages.MESSAGE_AUTHENTICATION_FAILED;
                responseError.messages.push(messages_1.Messages.MESSAGE_EMAIL_AND_OR_WRONG_PASSWORD);
                return res.status(api_util_1.APIUtil.UNAUTHORIZED).json(responseError);
            }
            else {
                const expire = Math.floor(Date.now() / 1000) + (600 * 600);
                const token = jwt.sign({
                    exp: expire,
                    data: {
                        name: user.name,
                        email: user.email
                    }
                }, "secretConfig.jwtSecret");
                res.status(api_util_1.APIUtil.OK).json({
                    access_token: token,
                    // refresh_token: base64data,
                    expire: expire
                });
            }
        })(req, res, next);
    }
}
exports.UserController = UserController;
//# sourceMappingURL=UserController.js.map