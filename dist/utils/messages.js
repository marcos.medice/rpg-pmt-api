"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Messages = void 0;
class Messages {
}
exports.Messages = Messages;
Messages.MESSAGE_INVALID_EMAIL = "E-mail inválido!";
Messages.MESSAGE_INVALID_DOCUMENT = "CPF inválido!";
Messages.MESSAGE_INVALID_PASSWORD = "Senha fraca!";
Messages.MESSAGE_TERMS_NOT_ACCEPTED = "Termos de uso não aceitos!";
Messages.MESSAGE_USER_ALREADY_REGISTERED = "Dados de usuário já registrado!";
Messages.GENERIC_ERROR_500_MESSAGE = "Desculpe, ocorreu um erro, favor entrar em contato com nosso suporte!";
Messages.MESSAGE_AUTHENTICATION_FAILED = "Falha na autenticação de usuário!";
Messages.MESSAGE_EMAIL_AND_OR_WRONG_PASSWORD = "E-mail ou senha inválidos!";
Messages.MESSAGE_USER_NOT_FOUND = "Usuário não encontrado!";
Messages.MESSAGE_USER_CHANGE_PASSWORD_REQUEST_NOT_FOUND = "Link inexistente ou expirado!";
Messages.MESSAGE_INVALID_IMAGE = "Não foi possível carregar a imagem!";
Messages.MESSAGE_WELCOME = "Seja bem vindo!";
Messages.MESSAGE_TO_ACTIVATE_YOUR_REGISTER_CLICK_HERE = "Para ativar seu cadastro clique aqui:";
Messages.MESSAGE_ACCOUNT_ACTIVATED_SUBJECT = "Conta PmT ativada com sucesso!";
Messages.MESSAGE_ACCOUNT_ACTIVATED_TEXT = "Sua conta foi ativada com sucesso! Aproveite nossa plataforma:";
Messages.MESSAGE_CHANGE_PASSWORD_REQUEST = "Recuperação da conta";
Messages.MESSAGE_TO_CHANGE_PASSWORD_CLICK_HERE = "Para trocar sua senha acesse o link a seguir:";
Messages.MESSAGE_PASSWORD_CHANGED_SUBJECT = "Senha alterada com sucesso!";
Messages.MESSAGE_PASSWORD_CHANGED_TEXT = "Sua senha foi alterada com sucesso! Sua nova senha é:";
Messages.MESSAGE_PASSWORD_NEEDED = "Para acessar é necessário senha!";
Messages.MESSAGE_CAMPAIGN_NOT_FOUND = "Campanha não encontrada!";
Messages.MESSAGE_CHARACTER_NOT_FOUND = "Personagem não encontrado!";
//# sourceMappingURL=messages.js.map