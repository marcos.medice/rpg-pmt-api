"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.APIUtil = void 0;
class APIUtil {
}
exports.APIUtil = APIUtil;
APIUtil.OK = 200;
APIUtil.UNAUTHORIZED = 401;
APIUtil.BAD_REQUEST = 400;
APIUtil.INTERNAL_SERVER_ERROR = 500;
//# sourceMappingURL=api-util.js.map