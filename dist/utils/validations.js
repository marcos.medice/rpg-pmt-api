"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Validations = void 0;
const cpf_1 = __importDefault(require("cpf"));
class Validations {
    validateEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return (/^[^!#%¨&*()+\s@]+@[^!#%¨&*()+\s@]+\.[^!#%¨&*()+\s@]+$/).test(email);
        });
    }
    validateStrongPassword(password) {
        return __awaiter(this, void 0, void 0, function* () {
            return (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#!%()])[0-9a-zA-Z$*&@#!%()]{8,}$/).test(password);
        });
    }
    validateDocument(document) {
        return __awaiter(this, void 0, void 0, function* () {
            return cpf_1.default.isValid(document);
        });
    }
}
exports.Validations = Validations;
//# sourceMappingURL=validations.js.map