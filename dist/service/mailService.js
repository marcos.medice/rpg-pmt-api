"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ses_1 = __importDefault(require("aws-sdk/clients/ses"));
class MailService {
    constructor() {
        this.params = {};
        this.client = new ses_1.default({
            region: "us-east-2",
        });
    }
    run(to, subject, text) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.client
                .sendEmail({
                Source: "<marcos.medice@gmail.com>",
                Destination: {
                    ToAddresses: [to],
                },
                Message: {
                    Subject: {
                        Data: subject,
                    },
                    Body: {
                        Text: {
                            Data: text,
                        },
                    },
                }
            }).promise();
        });
    }
    sendHtml(to, subject, content) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.client
                .sendEmail({
                Source: "PmT <marcos.medice@gmail.com>",
                Destination: {
                    ToAddresses: [to],
                },
                Message: {
                    Subject: {
                        Data: subject,
                    },
                    Body: {
                        Html: {
                            Charset: "UTF-8",
                            Data: content
                        }
                    },
                }
            })
                .promise();
        });
    }
}
exports.default = MailService;
//# sourceMappingURL=mailService.js.map