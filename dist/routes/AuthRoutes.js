"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoutes = void 0;
const express_1 = require("express");
const UserController_1 = require("../controllers/UserController");
class AuthRoutes {
    constructor() {
        this.userController = new UserController_1.UserController();
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        // For TEST only ! In production, you should use an Identity Provider !!
        this.router.post("", this.userController.authenticateUser);
    }
}
exports.AuthRoutes = AuthRoutes;
//# sourceMappingURL=AuthRoutes.js.map