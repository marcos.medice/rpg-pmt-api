"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CampaignRoutes = void 0;
const express_1 = require("express");
const CampaignController_1 = require("../controllers/CampaignController");
const AuthController_1 = require("../controllers/AuthController");
class CampaignRoutes {
    constructor() {
        this.campaignController = new CampaignController_1.CampaignController();
        this.authController = new AuthController_1.AuthController();
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.authController.authenticateJWT, this.campaignController.save);
    }
}
exports.CampaignRoutes = CampaignRoutes;
//# sourceMappingURL=CampaignRoutes.js.map