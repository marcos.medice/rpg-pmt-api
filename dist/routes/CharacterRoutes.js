"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CharacterRoutes = void 0;
const express_1 = require("express");
const CharacterController_1 = require("../controllers/CharacterController");
const AuthController_1 = require("../controllers/AuthController");
const multer = require("multer");
class CharacterRoutes {
    constructor() {
        this.characterController = new CharacterController_1.CharacterController();
        this.authController = new AuthController_1.AuthController();
        this.upload = multer({ dest: "./uploads/", rename: function (fieldname, filename) {
                return filename;
            },
        });
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.authController.authenticateJWT, this.characterController.saveCharacter);
        this.router.patch("/update-picture", this.upload.single("picture"), this.authController.authenticateJWT, this.characterController.updateCharacterPicture);
    }
}
exports.CharacterRoutes = CharacterRoutes;
//# sourceMappingURL=CharacterRoutes.js.map