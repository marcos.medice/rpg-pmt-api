"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRoutes = void 0;
const express_1 = require("express");
const UserController_1 = require("../controllers/UserController");
const AuthController_1 = require("../controllers/AuthController");
const multer = require("multer");
class UserRoutes {
    constructor() {
        this.upload = multer({ dest: "./uploads/", rename: function (fieldname, filename) {
                return filename;
            },
        });
        this.userController = new UserController_1.UserController();
        this.authController = new AuthController_1.AuthController();
        this.routes();
    }
    routes() {
        this.router = express_1.Router();
        this.router.post("", this.userController.saveUser);
        this.router.patch("/activate", this.userController.activateUser);
        this.router.patch("/update-profile-picture", this.upload.single("profilePicture"), this.authController.authenticateJWT, this.userController.updateProfilePicture);
        this.router.post("/change-password-request", this.userController.changePasswordRequest);
        this.router.patch("/:token/change-password", this.userController.updatePasswordFromRequest);
        this.router.patch("/update-profile", this.authController.authenticateJWT, this.userController.updateUserProfile);
    }
}
exports.UserRoutes = UserRoutes;
//# sourceMappingURL=UserRoutes.js.map