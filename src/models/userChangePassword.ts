import { Schema, Document, model, Model } from "mongoose";
import bcrypt from "bcrypt";

export interface IUserChangePassword extends Document {
  userId: String;
  token: String;
  status: Boolean;
  expireDate: Date;
  creationDate: Date;
  updateDate: Date;
}

export const userChangePasswordSchema = new Schema({
  userId: String,
  token: String,
  status: Boolean,
  expireDate: {
    type: Date,
    default: undefined
  },
  creationDate: {
    type: Date,
    default: Date.now
  },
  updateDate: {
    type: Date,
    default: undefined
  }
});

interface Options {
  virtuals?: boolean;
  hide?: string;
  transform?: any;
}

let options: Options;
options = {};

options.virtuals = true;
  
userChangePasswordSchema.set("toJSON", options);

userChangePasswordSchema.virtual("users", {
  ref: "Users",
  localField: "userId",
  foreignField: "_id",
  justOne: true
});

export const UserChangePassword: Model<IUserChangePassword> = model<IUserChangePassword>("UserChangePassword", userChangePasswordSchema, "userChangePasswordRequests");