import { Schema, Document, Model, model } from 'mongoose';
import bcrypt from 'bcrypt';

export interface ICampaign extends Document {
    master: String;
    name: String;
    uuid: String;
    isPrivate: Boolean;
    password: String;
    playersIds: Array<String>;
    charactersIds: Array<String>;
    worldLevel: Number;
    status: Boolean;
    creationDate: Date;
    updateDate: Date;
}

export const campaignSchema = new Schema({
    master: {
        type: Schema.Types.ObjectId, required: false, ref: "User"
    },
    name: String,
    uuid: String,
    isPrivate: Boolean,
    password: String,
    playersIds: [{
        type: Schema.Types.ObjectId, required: false, ref: "User"
    }],
    charactersIds: [{
        type: Schema.Types.ObjectId, required: false, ref: "User"
    }],
    worldLevel: {
        type: Number,
        default: 1
    },
    status: {
        type: Boolean,
        default: false
    },
    creationDate: {
        type: Date,
        default: new Date()
    },
    updateDate: {
        type: Date,
        default: undefined
    }
})

interface Options {
    virtuals?: boolean;
    hide?: string;
    transform?: any;
}

let options: Options = {};
options.virtuals = true;
options.transform = function (doc: any, ret: any, options: Options) {
    if (options.hide) {
        options.hide.split("password").forEach(function (prop: any) {
            delete ret[prop];
        });
    }
    return ret;
};

campaignSchema.set("toJSON", options);

campaignSchema.pre<ICampaign>("save", function save(next) {
    const campaign = this;
    next();
});

campaignSchema.methods.comparePassword = function <ICampaign>(candidatePassword: string, callback: any) {
    bcrypt.compare(candidatePassword, this.password, (err: Error, isMatch: boolean) => {
        callback(err, isMatch);
    });
};

export const Campaign: Model<ICampaign> = model<ICampaign>("Campaign", campaignSchema, "campaigns")