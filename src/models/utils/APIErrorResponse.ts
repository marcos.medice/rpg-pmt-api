export class APIErrorResponse {

    public errors: Array<string> = new Array<string>();
    public error: string;
    public messages: Array<string> = new Array<string>();

    constructor() {
    }

}
