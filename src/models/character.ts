import { Schema, Document, Model, model } from 'mongoose';

export interface ICharacter extends Document {
    userId: String;
    campaignId: String;
    name: String;
    uuid: String;
    picture: any;
    race: String;
    class: String;
    level: Number;
    stats: Object;
    equipment: Object;
    inventory: Object;
    status: String;
    creationDate: Date;
    updateDate: Date;
}

export const characterSchema = new Schema ({
    userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
    campaignId: { type: Schema.Types.ObjectId, required: true, ref: "Campaign"},
    name: String,
    uuid: String,
    picture: {
        data: Buffer,
        contentType: String
    },
    race: String,
    class: String,
    level: {
        type: Number,
        default: 1
    },
    stats: {
        type: Object,
        default: {
            HP: undefined,
            strenght: undefined,
            constitution: undefined,
            dexterity: undefined,
            intelligence: undefined,
            wisdom: undefined,
            charisma: undefined
        }
    },
    equipment: {
        type: Object,
        default: {
            head: undefined,
            neck: undefined,
            body: undefined,
            leftHand: {
                weapon: undefined,
                ring: undefined
            },
            rightHand: {
                weapon: undefined,
                ring: undefined
            },
            pants: undefined,
            legs: undefined,
            feet: undefined,
            extra: [],
            pets: []
        },
    },
    inventory: [],
    creationDate: {
        type: Date,
        default: new Date()
    },
    updateDate: {
        type: Date,
        default: undefined
    },
    status: String
})

interface Options {
    virtual?: boolean,
    hide?: string,
    transform?: any
}

let options: Options = {};
options.virtual = true;

characterSchema.set("toJSON", options);
characterSchema.pre<ICharacter>("save", function save(next) {
    const character = this;
    next();
});

export const Character: Model<ICharacter> = model<ICharacter>("Character", characterSchema, "character");