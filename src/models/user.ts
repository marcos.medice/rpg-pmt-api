import { Schema, Document, model, Model } from "mongoose";
import bcrypt from "bcrypt";

export interface IUser extends Document {
  name: String;
  username: String;
  email: String;
  document: String;
  cellPhone: String;
  password: String;
  acceptUseTerms: Boolean;
  status: Boolean;
  activationToken: String;
  profilePicture: any;
  creationDate: Date;
  activationDate: Date;
  updateDate: Date;
}

export const userSchema = new Schema({
  name: String,
  username: String,
  email: String,
  document: String,
  cellPhone: String,
  password: String,
  acceptUseTerms: Boolean,
  status: Boolean,
  activationToken: String,
  profilePicture: {
    data: Buffer,
    contentType: String
  },
  creationDate: {
    type: Date,
    default: Date.now
  },
  activationDate: {
    type: Date,
    default: undefined
  },
  updateDate: {
    type: Date,
    default: undefined
  }
});

interface Options {
  virtuals?: boolean;
  hide?: string;
  transform?: any;
}

let options: Options;
options = {};

options.virtuals = true;

options.transform = function (doc: any, ret: any, options: Options) {
  if (options.hide) {
    options.hide.split("password").forEach(function (prop: any) {
      delete ret[prop];
    });
  }
  return ret;
};
  
userSchema.set("toJSON", options);

userSchema.pre<IUser>("save", function save(next) {
  const user = this;

  user.email = user.email.toLowerCase();

  next();
});

userSchema.methods.comparePassword = function <IUser>(candidatePassword: string, callback: any) {
  bcrypt.compare(candidatePassword, this.password, (err: Error, isMatch: boolean) => {
    callback(err, isMatch);
  });
};
  

export const User: Model<IUser> = model<IUser>("User", userSchema, "users");