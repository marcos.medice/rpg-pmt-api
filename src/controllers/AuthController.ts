import { Request, Response, NextFunction } from "express";
import { APIUtil } from "../utils/api-util";
import "../auth/passportHandler";
import jwtSimple from "jwt-simple";
import passport from "passport";

export class AuthController {

    public authenticateJWT(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("jwt", function (err, user, info) {
            if (err) {
                return res.status(APIUtil.UNAUTHORIZED).json();
            }

            if (!user) {
                return res.status(APIUtil.UNAUTHORIZED).json();
            } else {
                req.user = user;
                return next();
            }
        }) (req, res, next);
    }

    public authorizeJWT(req: Request, res: Response, next: NextFunction) {

        passport.authenticate("jwt", (err, user, jwtToken) => {
            if (err) {
                return res.status(APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized"});
            }

            if (!user) {
                return res.status(APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized"});
            } else {
                const scope = req.baseUrl.split("/").slice(-1)[0];
                const authScope = jwtToken.scope;

                if (authScope && authScope.indexof(scope) > -1) {
                    return next();
                } else {
                    return res.status(APIUtil.UNAUTHORIZED).json({ status: "error", code: "unauthorized"});
                }
            }
        }) (req, res, next);
    }
}