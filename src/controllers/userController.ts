import { Request, Response, NextFunction } from "express";
import { User, IUser } from "../models/user";
import { UserChangePassword, IUserChangePassword } from "../models/userChangePassword";
import bcrypt, { genSaltSync } from "bcrypt";
import { APIErrorResponse } from "../models/utils/APIErrorResponse";
import { APIResponse } from "../models/utils/APIResponse";
import { APIUtil } from "../utils/api-util";
import { Validations } from "../utils/validations";
import { Messages } from "../utils/messages";
import { v4 as uuidv4 } from "uuid";
import passport from "passport";
import * as jwt from "jsonwebtoken";
import * as fs from "fs";
import MailService from "../service/mailService";

const multer = require("multer");
const secretConfig = require("../config/secret");

export class UserController {

    public async saveUser(req: Request, res: Response): Promise<void> {
        const responseError: APIErrorResponse = new APIErrorResponse();
        const validations: Validations = new Validations();

        try {

            const user: IUser = new User();

            if (!await validations.validateEmail(req.body.email)) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_EMAIL});
                return;
            }

            if (!await validations.validateDocument(req.body.document)) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_DOCUMENT});
                return;
            }

            if (!await validations.validateStrongPassword(req.body.password)) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_PASSWORD});
                return;
            }

            if (!req.body.acceptUseTerms) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_TERMS_NOT_ACCEPTED});
                return;
            }

            const query = {$or: [
                { document: req.body.document },
                { email: req.body.email },
                { username: req.body.username },
                { cellPhone: req.body.cellPhone }
                ]};
        
            const isRegisterValid = await User.find(query);
        
            if (isRegisterValid && isRegisterValid.length > 0) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_ALREADY_REGISTERED });
                return;
            }
        
            user.name = req.body.name;
            user.username = req.body.username;
            user.email = req.body.email;
            user.document = req.body.document;
            user.cellPhone = req.body.cellPhone;
            user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
            user.acceptUseTerms = true;
            user.status = false;
            user.activationToken = uuidv4();

            User.create(user);

            const mailService = new MailService();
            const activationUrl: string = "http://localhost:PORT/users/activate/";
            let htmlEmail = `${Messages.MESSAGE_WELCOME} ${Messages.MESSAGE_TO_ACTIVATE_YOUR_REGISTER_CLICK_HERE} ${activationUrl}${user.activationToken}`;
            await mailService.run(req.body.email, Messages.MESSAGE_WELCOME, htmlEmail);

            res.status(APIUtil.OK).json("ok");

        } catch (error) {

            responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
            res.status(APIUtil.INTERNAL_SERVER_ERROR).json(responseError);
        }
    }

    public async activateUser(req: Request, res: Response): Promise<void> {
      const responseError: APIErrorResponse = new APIErrorResponse();

      try {

        const user: IUser = await User.findOne({ activationToken: req.query.activationToken.toString(), status: false });

        if (!user) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
          return;
        }

        user.status = true;
        user.activationDate = new Date();
        user.activationToken = undefined;

        user.save();

        const mailService = new MailService();
        const platformUrl = "http://localhost:PORT/login";
        let htmlEmail = `${Messages.MESSAGE_ACCOUNT_ACTIVATED_TEXT} ${platformUrl}`;
        await mailService.run(user.email.toString(), Messages.MESSAGE_ACCOUNT_ACTIVATED_SUBJECT, htmlEmail);

        res.status(APIUtil.OK).json("ok");
  
      } catch (error) {
        responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
        res.status(APIUtil.BAD_REQUEST).json(error);
      }

    }

    public async updateProfilePicture(req: Request, res: Response): Promise<void> {
      const responseError: APIErrorResponse = new APIErrorResponse();

      try {
        
        const user: IUser = await User.findOne({ _id: req.body.id, status: true });

        if (!user) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
          return;
        }

        user.profilePicture.data = fs.readFileSync(req.file.path);
        user.profilePicture.contentType = req.file.mimetype;
        user.updateDate = new Date();
        user.save();

        fs.unlinkSync(req.file.path);

        res.status(APIUtil.OK).json("ok");

      } catch (error) {
        responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
        res.status(APIUtil.BAD_REQUEST).json(error);
      }
    }

    public async changePasswordRequest(req: Request, res: Response): Promise<void> {
      const responseError: APIErrorResponse = new APIErrorResponse();

      try {
        const user: IUser = await User.findOne({ email: req.body.email, status: true });

        if (!user) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
          return;
        }

        const userChangePassword: IUserChangePassword = new UserChangePassword();
        userChangePassword.userId = user._id;
        userChangePassword.token = uuidv4();
        userChangePassword.status = true;
        // userChangePassword.expireDate =
        UserChangePassword.create(userChangePassword);

        const mailService = new MailService();
        const changePasswordUrl = "http://localhost:PORT/users/";
        let htmlEmail = `${Messages.MESSAGE_TO_CHANGE_PASSWORD_CLICK_HERE} ${changePasswordUrl}${userChangePassword.token}/change-password`;
        await mailService.run(req.body.email, Messages.MESSAGE_CHANGE_PASSWORD_REQUEST, htmlEmail);

        res.status(APIUtil.OK).json("ok");

      } catch (error) {
        responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
        res.status(APIUtil.BAD_REQUEST).json(error);
      }
    }

    public async updatePasswordFromRequest(req: Request, res: Response): Promise<void> {
      const responseError: APIErrorResponse = new APIErrorResponse();
      const validations: Validations = new Validations();

      try {

        if (!await validations.validateStrongPassword(req.body.password)) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_PASSWORD });
          return;
        }

        const userChangePassword: IUserChangePassword = await UserChangePassword
          .findOne({ token: req.params.token, status: true })

        if (!userChangePassword) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error",
            message: Messages.MESSAGE_USER_CHANGE_PASSWORD_REQUEST_NOT_FOUND });
          return;
        }

        const user: IUser = await User.findOne({ _id: userChangePassword.userId, status: true });

        if (!user) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
          return;
        }

        userChangePassword.status = false;
        userChangePassword.expireDate = undefined;
        userChangePassword.updateDate = new Date();
        userChangePassword.save();

        user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
        user.updateDate = new Date();
        user.save();

        res.status(APIUtil.OK).json("ok");

        const mailService = new MailService();
        let htmlEmail = `${Messages.MESSAGE_PASSWORD_CHANGED_TEXT} ${req.body.password}`;
        await mailService.run(user.email.toString(), Messages.MESSAGE_PASSWORD_CHANGED_SUBJECT, htmlEmail);

      } catch (error) {
        responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
        res.status(APIUtil.BAD_REQUEST).json(error);
      }
    }

    public async updateUserProfile(req: Request, res: Response): Promise<void> {
      const responseError: APIErrorResponse = new APIErrorResponse();
      const validations: Validations = new Validations();

      try {
        
        const user: IUser = await User.findOne({ _id: req.body.id, status: true });

        if (!user) {
          res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
          return;
        }

        if (req.body.password) {
          if (!await validations.validateStrongPassword(req.body.password)) {
            res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_PASSWORD });
            return;
          } else {
            user.password = bcrypt.hashSync(req.body.password, genSaltSync(10));
          }
        }

        if (req.body.email) {
          if (!await validations.validateEmail(req.body.email)) {
            res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_INVALID_EMAIL });
            return;
          } else {
            user.email = req.body.email;
          }
        }

        if (req.body.username) { user.username = req.body.username }

        if (req.body.cellPhone) { user.cellPhone = req.body.cellPhone }

        // if (req.file) {
        //   user.profilePicture.data = fs.readFileSync(req.file.path);
        //   user.profilePicture.contentType = req.file.mimetype;
        //   fs.unlinkSync(req.file.path);
        // }

        user.updateDate = new Date();
        user.save();

        res.status(APIUtil.OK).json("ok");

      } catch (error) {
        responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
        res.status(APIUtil.BAD_REQUEST).json(error);
      }
    }

    public authenticateUser(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("local", (err: Error, user: any, info: any) => {
          if (err) return next(err);
          if (!user) {
            const responseError: APIErrorResponse = new APIErrorResponse();
            responseError.error = Messages.MESSAGE_AUTHENTICATION_FAILED;
            responseError.messages.push(Messages.MESSAGE_EMAIL_AND_OR_WRONG_PASSWORD);
    
            return res.status(APIUtil.UNAUTHORIZED).json(responseError);
          } else {
    
            const expire = Math.floor(Date.now() / 1000) + (600 * 600);
            const token = jwt.sign({
              exp: expire,
              data: {
                name: user.name,
                email: user.email
              }
    
            }, "secretConfig.jwtSecret");
            res.status(APIUtil.OK).json({
              access_token: token,
              // refresh_token: base64data,
              expire: expire
            });
          }
        })(req, res, next);
      }
}