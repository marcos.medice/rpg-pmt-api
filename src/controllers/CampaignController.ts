import { Request, Response } from 'express';
import { Campaign, ICampaign } from '../models/campaign';
import { APIUtil } from '../utils/api-util';
import { Messages } from '../utils/messages';
import { APIErrorResponse } from '../models/utils/APIErrorResponse';
import { APIResponse } from '../models/utils/APIResponse';
import { v4 as uuidv4 } from 'uuid';
import bcrypt from 'bcrypt';

export class CampaignController {

    public async save(req: Request, res: Response): Promise<void> {
        const responseError: APIErrorResponse = new APIErrorResponse();
        
        try {
            const campaign: ICampaign = new Campaign();
            campaign.master = req.body.id;
            campaign.name = req.body.name;
            campaign.uuid = uuidv4();
            campaign.isPrivate = req.body.isPrivate;

            if (req.body.isPrivate) {

                if (!req.body.password) {
                    res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_PASSWORD_NEEDED });
                    return;
                }

                campaign.password = req.body.password;
            }

            campaign.playersIds.push(req.body.id)

            
            Campaign.create(campaign);
            res.status(APIUtil.OK).json("ok");
    
        } catch (error) {
            responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
            res.status(APIUtil.INTERNAL_SERVER_ERROR).json(responseError);
        }
    }
}