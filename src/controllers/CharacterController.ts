import { Request, Response } from 'express';
import { APIUtil } from '../utils/api-util';
import { Messages } from '../utils/messages';
import { APIErrorResponse } from '../models/utils/APIErrorResponse';
import { APIResponse } from '../models/utils/APIResponse';
import { Character, ICharacter } from '../models/character';
import { Campaign } from '../models/campaign';
import { User } from '../models/user';
import { v4 as uuidv4 } from 'uuid';
import * as fs from 'fs';

export class CharacterController {

    public async saveCharacter(req: Request, res: Response): Promise<void> {
        const responseError: APIErrorResponse = new APIErrorResponse();

        try {

            if (!await User.findById(req.body.userId)) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_USER_NOT_FOUND });
                return;
            }
            
            
            if (!await Campaign.findById(req.body.campaignId)) {
                res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_CAMPAIGN_NOT_FOUND });
                return;
            }

            const character: ICharacter = new Character();
            character.userId = req.body.userId;
            character.campaignId = req.body.campaignId;
            character.name = req.body.name;
            character.uuid = uuidv4();
            character.race = req.body.race;
            character.class = req.body.class;
            Character.create(character);
            
            res.status(APIUtil.OK).json("ok");

        } catch (error) {
            responseError.errors.push(error);
            res.status(APIUtil.INTERNAL_SERVER_ERROR).json({ status: "error", message: Messages.GENERIC_ERROR_500_MESSAGE })
        }
    }

    public async updateCharacterPicture(req: Request, res: Response): Promise<void> {
        const responseError: APIErrorResponse = new APIErrorResponse();
  
        try {
          
          const character: ICharacter = await Character.findOne({ _id: req.body.id });
  
          if (!character) {
            res.status(APIUtil.BAD_REQUEST).json({ status: "error", message: Messages.MESSAGE_CHARACTER_NOT_FOUND });
            return;
          }
  
          character.picture.data = fs.readFileSync(req.file.path);
          character.picture.contentType = req.file.mimetype;
          character.updateDate = new Date();
          character.save();
  
          fs.unlinkSync(req.file.path);
  
          res.status(APIUtil.OK).json("ok");
  
        } catch (error) {
          responseError.errors.push(Messages.GENERIC_ERROR_500_MESSAGE);
          res.status(APIUtil.BAD_REQUEST).json(error);
        }
    }
}