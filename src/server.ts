import mongoConfig from "./config/mongo";
import "dotenv/config";

import express from "express";
import cors from "cors";
import compression from "compression";
import mongoose from "mongoose";
import morgan from "morgan";
import swaggerUi from "swagger-ui-express";
import * as swaggerDocument from "./config/swagger/swagger.json";
const multer = require("multer");
import passport from "passport";

import { UserRoutes } from "./routes/UserRoutes";
import { AuthRoutes } from "./routes/AuthRoutes";
import { CampaignRoutes } from "./routes/CampaignRoutes";
import { CharacterRoutes } from "./routes/CharacterRoutes";

var AWS = require("aws-sdk");

export class Server {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
        this.mongo();
    }

    public config(): void {

        this.app.set("port", process.env.PORT || 3000);
        this.app.use(morgan(":remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] - :response-time ms"));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(compression());
        this.app.use(cors());
        this.app.use(passport.initialize());
        this.app.use(passport.session());
        require("./auth/passportHandler");
        this.app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    }

    public routes(): void {
        this.app.use("/api/v1/auth", new AuthRoutes().router);
        this.app.use("/api/v1/users", new UserRoutes().router);
        this.app.use("/api/v1/campaign", new CampaignRoutes().router);
        this.app.use("/api/v1/character", new CharacterRoutes().router);
    }

    private mongo() {

        const connection = mongoose.connection;

        mongoose.set("useFindAndModify", false);

        connection.on("connected", () => {
            console.log("Mongo Connection Established");
        });
        connection.on("reconnected", () => {
            console.log("Mongo Connection Reestablished");
        });
        connection.on("disconnected", () => {
            console.log("Mongo Connection Disconnected");
        });
        connection.on("error", (error) => {
            console.log("Mongo Connection ERROR:" + error);
        });

        const run = async () => {
            await mongoose.connect(mongoConfig.connection, {
                autoReconnect: true, keepAlive: true, connectTimeoutMS: 10000, maxPoolSize: 10, autoIndex: true
            });
        };

        run().catch(error => console.log(error));

    }

    public start(): void {

        this.app.listen(this.app.get("port"), () => {
            return console.log("\x1b[33m%s\x1b[0m", "Server :: Running @ 'http://localhost:'" + this.app.get("port"));
          });
    }
}

const server: Server = new Server();

server.start();