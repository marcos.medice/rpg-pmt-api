import { Router } from 'express';
import { CampaignController } from '../controllers/CampaignController';
import { AuthController } from '../controllers/AuthController';

export class CampaignRoutes {

    public router: Router;
    public campaignController: CampaignController = new CampaignController();
    public authController: AuthController = new AuthController();

    constructor() { 
        this.routes()
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.authController.authenticateJWT, this.campaignController.save)
    }    
}