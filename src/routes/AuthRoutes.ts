import { Router } from "express";
import { UserController } from "../controllers/UserController";

export class AuthRoutes {

    router: Router;
    public userController: UserController = new UserController();

    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {
        // For TEST only ! In production, you should use an Identity Provider !!
        this.router.post("", this.userController.authenticateUser);
    }
}
