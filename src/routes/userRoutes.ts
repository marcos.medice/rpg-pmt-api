import { Router } from "express";
import { UserController } from "../controllers/UserController";
import { AuthController } from "../controllers/AuthController";
const multer = require("multer");

export class UserRoutes {

    // TODO: AUTHENTICATION JWT + PASSPORT

    public router: Router;
    public upload = multer({ dest: "./uploads/",
        rename: function (fieldname: string, filename: string) {
          return filename;
        },
    });

    public userController: UserController = new UserController();
    public authController: AuthController = new AuthController();

    constructor() {
        this.routes();
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.userController.saveUser);
        this.router.patch("/activate", this.userController.activateUser);
        this.router.patch("/update-profile-picture", 
            this.upload.single("profilePicture"),
            this.authController.authenticateJWT, 
            this.userController.updateProfilePicture);
        this.router.post("/change-password-request", this.userController.changePasswordRequest);
        this.router.patch("/:token/change-password", this.userController.updatePasswordFromRequest);
        this.router.patch("/update-profile", this.authController.authenticateJWT, this.userController.updateUserProfile);
    }
}