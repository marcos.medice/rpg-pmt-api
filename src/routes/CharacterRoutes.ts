import { Router } from 'express';
import { CharacterController } from '../controllers/CharacterController';
import { AuthController } from '../controllers/AuthController';
const multer = require("multer");

export class CharacterRoutes {

    public router: Router;
    public characterController: CharacterController = new CharacterController();
    public authController: AuthController = new AuthController();

    public upload = multer({ dest: "./uploads/",
        rename: function (fieldname: string, filename: string) {
          return filename;
        },
    });

    constructor() {
        this.routes();
    }

    public routes(): void {
        this.router = Router();
        this.router.post("", this.authController.authenticateJWT, this.characterController.saveCharacter);
        this.router.patch("/update-picture", 
            this.upload.single("picture"),
            this.authController.authenticateJWT,
            this.characterController.updateCharacterPicture);
    }
}