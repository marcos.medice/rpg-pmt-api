import SES from "aws-sdk/clients/ses";

class MailService {
    private client: SES;
    public params = {};

    constructor() {
        this.client = new SES({
            region: "us-east-2",
        });
    }


    async run(to: string, subject: string, text: string): Promise<void> {
        await this.client
        .sendEmail({
            Source: "<marcos.medice@gmail.com>",
            Destination: {
            ToAddresses: [to],
            },
            Message: {
            Subject: {
                Data: subject,
            },
            Body: {
                Text: {
                Data: text,
                },
            },
            }
        }).promise();
    }

    async sendHtml(to: string, subject: string, content: string): Promise<void> {
        await this.client
          .sendEmail({
            Source: "PmT <marcos.medice@gmail.com>",
            Destination: {
              ToAddresses: [to],
            },
            Message: {
              Subject: {
                Data: subject,
              },
              Body: {
                Html: {
                  Charset: "UTF-8",
                  Data: content
                }
              },
            }
          })
          .promise();
      }
    
}

export default MailService;
