import cpf from "cpf";

export class Validations {

    public async validateEmail(email: string): Promise<Boolean> {
        return (/^[^!#%¨&*()+\s@]+@[^!#%¨&*()+\s@]+\.[^!#%¨&*()+\s@]+$/).test(email);
    }

    public async validateStrongPassword(password: string): Promise<Boolean> {
        return (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#!%()])[0-9a-zA-Z$*&@#!%()]{8,}$/).test(password);
    }
      
    public async validateDocument(document: string): Promise<Boolean> {

        return cpf.isValid(document);
    }
      
}