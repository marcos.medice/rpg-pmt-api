export class APIUtil {
    
    public static OK = 200;
    public static UNAUTHORIZED = 401;
    public static BAD_REQUEST = 400;
    public static INTERNAL_SERVER_ERROR = 500;
    
}
