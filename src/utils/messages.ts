export class Messages {

    public static MESSAGE_INVALID_EMAIL = "E-mail inválido!";
    public static MESSAGE_INVALID_DOCUMENT = "CPF inválido!";
    public static MESSAGE_INVALID_PASSWORD = "Senha fraca!";
    public static MESSAGE_TERMS_NOT_ACCEPTED = "Termos de uso não aceitos!";
    public static MESSAGE_USER_ALREADY_REGISTERED = "Dados de usuário já registrado!";
    public static GENERIC_ERROR_500_MESSAGE = "Desculpe, ocorreu um erro, favor entrar em contato com nosso suporte!";
    public static MESSAGE_AUTHENTICATION_FAILED = "Falha na autenticação de usuário!";
    public static MESSAGE_EMAIL_AND_OR_WRONG_PASSWORD = "E-mail ou senha inválidos!";
    public static MESSAGE_USER_NOT_FOUND = "Usuário não encontrado!";
    public static MESSAGE_USER_CHANGE_PASSWORD_REQUEST_NOT_FOUND = "Link inexistente ou expirado!";
    public static MESSAGE_INVALID_IMAGE = "Não foi possível carregar a imagem!";
    public static MESSAGE_WELCOME = "Seja bem vindo!";
    public static MESSAGE_TO_ACTIVATE_YOUR_REGISTER_CLICK_HERE = "Para ativar seu cadastro clique aqui:";
    public static MESSAGE_ACCOUNT_ACTIVATED_SUBJECT = "Conta PmT ativada com sucesso!";
    public static MESSAGE_ACCOUNT_ACTIVATED_TEXT = "Sua conta foi ativada com sucesso! Aproveite nossa plataforma:";
    public static MESSAGE_CHANGE_PASSWORD_REQUEST = "Recuperação da conta";
    public static MESSAGE_TO_CHANGE_PASSWORD_CLICK_HERE = "Para trocar sua senha acesse o link a seguir:";
    public static MESSAGE_PASSWORD_CHANGED_SUBJECT = "Senha alterada com sucesso!";
    public static MESSAGE_PASSWORD_CHANGED_TEXT = "Sua senha foi alterada com sucesso! Sua nova senha é:";
    public static MESSAGE_PASSWORD_NEEDED = "Para acessar é necessário senha!";
    public static MESSAGE_CAMPAIGN_NOT_FOUND = "Campanha não encontrada!";
    public static MESSAGE_CHARACTER_NOT_FOUND = "Personagem não encontrado!";
}