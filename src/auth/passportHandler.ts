import { User, IUser } from "../models/user";
import passport from "passport";
import passportJwt from "passport-jwt";
import passportLocal from "passport-local";

const secretConfig = require("../config/secret");

const LocalStrategy = passportLocal.Strategy;
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

passport.use("local", new LocalStrategy({ usernameField: "email", passwordField: "password" }, (email, password, done) => {
  User.findOne({ email: email.toLowerCase(), status: true }, (err: Error, user: any) => {
    if (err) { return done(err); }
    if (!user) {
      return done(undefined, false, { message: `email ${email} not found.` });
    }
    user.comparePassword(password, (err: Error, isMatch: boolean) => {
      if (err) { return done(err); }
      if (isMatch) {
        return done(undefined, user);
      }
      return done(undefined, false, { message: "Invalid email or password." });
    });
  });
}));

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: "secretConfig.jwtSecret"
};

passport.use(new JwtStrategy(opts, function (jwtToken, done) {
    User.findOne({ email: jwtToken.data.email }, function (err: Error, user: IUser) {
      if (err) { return done(err, false); }
      if (user) {
        return done(undefined, user, jwtToken);
      } else {
        return done(undefined, false);
      }
    });
  }));
